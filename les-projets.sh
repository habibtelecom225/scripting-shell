#PROJET 1 (METHODE 1)
#!/bin/bash

function debut(){
	echo "Tappez la touche ENTRER  pour continuer"
	read
}
debut

function verification_existence(){
	read -p "Saississez le nom de l'utilisateur à verifier :" NOM
	if  grep "$NOM": /etc/passwd > /dev/null 
	then
		echo "L'utilisateur $NOM existe déja"
	else
		echo "L'utilisateur $NOM n'existe pas"
	fi
	debut
}
verification_existence

#PROJET 1 (METHODE 2)
#!/bin/bash

function pausse(){
        echo "tapper la touche d'ENTRE pour continuer"
        read
}
pausse

function verification_existence(){
        NOM_DES_UTILISATEURS={cat/etc/passwd  | cut -d:-f}
        for UTILISATEUR in $NOM_DES_UTILISATEURS
        do
                if [ $1=$UTILISATEUR ]
                then
                        return 1
                fi
        done
}
verification_existence

read -p "Donnez le nom de l'utilisateur à verifier :" NOM
verification_existence $NOM
if [ $? -eq 1 ]
then
        echo " l'utilisateur $NOM exist déjà"
        exit
else
        echo " l'utlisateur $NOM n'existe pas"
fi


#PROJET 2 (METHODE 1)
#!/bin/bash

function existence_groupe(){
        LISTEGROUPE={sudo cat /etc/group | cut -d: -f1}
        for GROUPE in $LISTEGROUPE
        do
                if [ $1=$GROUPE ]
                then
                        return 1
                fi
        done
}
existence_groupe

read -p "Ecrivez le nom du groupe :" NOMGROUPE
existence_groupe $NOMGROUPE
if [ $?=1 ]
then
        echo "Le groupe $NOMGROUPE existe déjà"
        exit 1
else
        echo "Le groupe $NOMGROUPE n'existe pas"
fi

#PROJET 2 (METHODE 2)
#!/bin/bash

function debut(){
        echo "Tappez la touche ENTRER  pour continuer"
        read
}
debut

function verification_existence(){
        read -p "Saississez le nom de de groupe à verifier :" GROUPE
        if  grep "$GROUPE": /etc/group > /dev/null
        then
                echo "Le groupe $GROUPE existe déja"
        else
                echo "Le groupe $GROUPE n'existe pas"
        fi
        debut
}
verification_existence


#PROJET 3 (Methode 1)
#!/bin/bash

function pausse
{
        echo "tapper la touche d'ENTRE pour continuer"
        read
}
pausse

function verifier(){
        read -p "Saissir le nom d'utilisateur afin de verifier s'il existe ou pas :" NOM
        if grep "$NOM:" /etc/passwd > /dev/null
        then
                echo "L'utilisateur $NOM exist déja"
        else
                echo "L'utilisateur $NOM n'existe pas"
        fi
}
verifier

A=1
while [ $A=1 ]
do
clear
echo "1.Creaction d'un utilisateur"
echo "2.Suppression d'un utilisateur"
read -p "VOICI LES MENUS , MERCI DE CHOISIR UN NUMERO ou tappe Q pour quitter :" CHOIX

case "$CHOIX" in
        1)
                verifier
                sudo adduser $NOM
                echo "Donc il a été bien créé avec succès"
                pausse
                ;;
        2)
                verifier
                sudo deluser $NOM
                echo "Et il a été supprimé avec succes"
                pausse
                ;;
        q)
                echo " Merci pour votre visite , et Aurevoir ..!"
                pausse
                exit 1
                ;;
        *)
                echo "Erreur de choix"
                pausse
                ;;
esac
done

#PROJET 3 (Methode 1)
#!/bin/bash

function pausse(){
        echo "tapper la touche d'ENTRE pour continuer"
        read
}
pausse

function verification_existence(){
        NOM_DES_UTILISATEURS={cat /etc/passwd  | cut -d:-f1}
        for UTILISATEUR in $NOM_DES_UTILISATEURS
        do
                if [ $1=$UTILISATEUR ]
                then
                        return 1
                fi
        done
}
verification_existence

clear
echo "1.Creaction d'un utilisateur"
echo "2.Suppression d'un utilisateur"
read -p "VOICI LES MENUS , MERCI DE CHOISIR UN NUMERO :" CHOIX

case $CHOIX in
1)
                echo "Donnez le nom d'utilisateur à crée"
                read NOM
                verification_existence $NOM
                if [ $? -eq 1 ]
                then
                        echo "L'utilisateur $NOM existe déjà"
                else
                        sudo useradd $NOM
                        echo "L'utilisateur $NOM a été bien crée"
                fi
                ;;
        2)
                echo "Donnez le nom de l'utilisateur à supprimer"
                read NOM
                verification_existence $NOM
                if [ $? -eq 1 ]
                then
                        sudo userdel $NOM
                        echo "L'utilisateur $NOM a été bien supprimé"
                else
                        echo "L'utilisateur $NOM n'existe pas"
                fi
                ;;
        *)
                echo "Erreur de choix"
		;;
		pausse
esac


#PROJET 4
#!/bin/bash

function pausse
{
        echo "tapper la touche d'ENTRE pour continuer"
        read
}
pausse

function verifier_existence_de_groupe(){
        read -p "Saissir le nom de groupe afin de verifier s'il existe ou pas :" GROUPE
        if grep "$GROUPE:" /etc/group > /dev/null
        then
                echo "le $GROUPE exist déja"
        else
                echo "Le $GROUPE n'existe pas"
        fi
}
verifier_existence_de_groupe

G=1
while [ $G=1 ]
do
clear
echo "1.Creaction de groupe"
echo "2.Suppression de groupe"
read -p "VOICI LES MENUS , MERCI DE CHOISIR UN NUMERO ou tappe Q pour quitter :" CHOIX

case "$CHOIX" in
        1)
                verifier_existence_de_groupe
                sudo addgroup $GROUPE
                echo "Donc il a été bien créé avec succès"
                pausse
                ;;
        2)
                verifier_existence_de_groupe
                sudo delgroup $GROUPE
                echo "Et il a été supprimé avec succes"
                pausse
                ;;
        q)
                echo " Merci pour votre visite , et Aurevoir ..!"
                exit 1
                ;;
        *)
                echo "Erreur de choix"
                pausse
                ;;
esac
done


#PROJET 5
#!/bin/bash

function pause(){
        echo "entrez la touche ENTRER pour continuer"
        read
}
pause

function verificationfichier(){
        ls $1 2> /dev/null
        if [ $? -eq 0 ]
        then
                return 0
        else
                return 1
        fi
}
verificationfichier

B=1
while [ $B=1 ]
do

echo "VOICI LES MENUS :"
echo ""
echo "1.Creer un fichier"
echo "2.Supprimer un fichier"
echo ""
read -p "FAITES UN CHOIX DE NUMERO , OU TU TAPPE "Q" POUR QUITTER :" CHOIX

        case $CHOIX in
                1)
                        read -p "Donner le nom de fichier à creer :" FICHIER
                        verificationfichier $FICHIER
                        if [ $? -eq 0 ]
                        then
                                echo "le fichier $FICHIER existe déja"
                                pause
                        else
                                sudo touch $FICHIER
                                echo "le fichier $FICHIER a été bien crée avec succès"
                        fi
                                ;;
                2)
                        read -p "Donner le nom de fichier à supprimer :" FICHIER
                        verificationfichier $FICHIER
                        if [ $? -eq 0 ]
                        then
                                sudo rm -Rf $FICHIER
                                echo "le fichier $FICHIER a été bien supprimé avec succes"
                        else
                                echo "Le fichier $FICHIER n'existe pas"
                                pause
                        fi
                        ;;
                q)
                        echo "Merci pour votre visites et Aurevoir"
                        exit
                        ;;
                *)
                        echo "Erreur de numero"
                        pause
                        ;;
        esac
done


#PROJET 6
#!/bin/bash

function pause(){
        echo "entrez la touche ENTRER pour continuer"
        read
}
pause

function verificationfichier_existe(){
        ls $1 2> /dev/null
        if [ $? -eq 0 ]
        then
                return 0
        else
                return 1
        fi
}
verificationfichier_existe

read -p "QUELLE EST ADRESSE OU NOM DE FIHCIER DONT LE DROIT VA ETRE MODIFIER :" FICHIER
verificationfichier_existe $FICHIER
if [ $? -eq 0 ]
then
        echo "Quelle est le nom de l'utilisateur proprietaire de fihcier"
        read HABIB
        echo "Quel est le nom du groupe de proprietaire ?"
        read CAMARA
        chown $HABIB:$CAMARA $FICHIER
        echo ""
        echo "les droits de propeitaire $HABIB sur le fichier $FICHIER a été bien modifier"
        pause
        exit
else
        echo "le fichier $FICHIER n'existe pas"
        exit
fi


#PROJET 7
#!/bin/bash

function pause(){
        echo "entrez la touche ENTRER pour continuer"
        read
}
pause

function verificationfichier_existe(){
        ls $1 2> /dev/null
        if [ $? -eq 0 ]
        then
                return 0
        else
                return 1
        fi
}
verificationfichier_existe

read -p "QUELLE EST ADRESSE OU NOM DE FIHCIER DONT LE DROIT VA ETRE MODIFIER :" FICHIER
verificationfichier_existe $FICHIER
if [ $? -eq 0 ]
then
        echo "le fichier $FICHIER existe bien"
else
	        echo "le fichier $FICHIER n'existe pas"
        pause
        exit
fi

clear
echo "Donnez les modification des droits du proprietaire (r lecture; w ecriture ; x execution) :"
read DROIT_DU_PROPRIETAIRE
chmod u+$DROIT_DU_PROPRIETAIRE $FICHIER
echo ""
echo " les droit $DROIT_DU_PROPRIETAIRE proprietaire a été bien modifier sur le fichier $FICHIER"

echo ""
echo ""
echo "Donnez les modification des droits du groupe (r lecture; w ecriture ; x execution) :"
read DROIT_DU_GROUPE
chmod g+$DROIT_DU_GROUPE $FICHIER
echo ""
echo "les droits $DROIT_DU_GROUPE de groupe a été bien modifier sur le fichier $FICHIER"

echo""
echo""
echo "Donnez les modification des droits des autres (r lecture; w ecriture ; x execution) :"
read DROIT_DES_AUTRES
chmod o+$DROIT_DES_AUTRES $FICHIER
echo ""
echo "Les droits $DROIT_DES_AUTRES des autres a été bien modifé sur le fichier $FICHIER"
echo ""
