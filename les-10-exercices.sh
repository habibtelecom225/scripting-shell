#EXERCICE 1
#!/bin/bash
echo "Donnez votre date de naissance"
echo "ou le nombres de cadeaux reçu"
echo "ou votre jours de naissance"

read TEXTE
if [ $TEXTE = "1.01.1981" ]
then
        echo "votre date de naissance est bien $TEXTE"
elif [ $TEXTE = "5 cadeaux" ]
then
        echo "Tu as bien reçu $TEXTE"
elif [ $TEXTE = "samedi" ]
then
        echo "le jours de votre naissance est bien le $TEXTE"
else
        echo "echec"
fi


#EXERCICE 2
#!/bin/bash
if [ $# -ne 0 ]
then
	for A in $@
	do
		echo "$A"
	done
else
	echo "sans argument"
fi

#EXERCICE 3
#!/bin/bash
if [ $1 -eq 0 ]
then
	echo "aucun parametre sur la ligne de commande"
elif [ -f $1 ]
then
	A=$(cat $1)
	if [ -s $A ]
	then
		echo "c'est un fichier vide"
	else
		for B in $A
		do
			echo "$B"
		done
	fi
else
	echo "ce n'est pas un fichier sur la ligne commande"
fi

#EXERCICE 4
#!/bin/bash

if [ $# -eq 0 ]
then
        echo "il n'ya aucun argument sur la ligne de commande"
else
        A=$(ls $1)
        for B in $A
        do
                if [ -d $B ]
                then
                        echo "voici la liste sous-les repertoires du repertoire$1:" $B
                else
                        echo "ce n'est pas un repertoire à la commande"
                fi
        done
fi

#EXERCICE 5
#!/bin/bash

w={who | grep $1}
if [ -z "$w" ]
then
        echo "$1 n'est n'est pas connecté"
fi
#Ce programme determine si l'utilisateur dont le nom est donnée comme argument est connecté ou pas.

#EXERCICE 6
#!/bin/bash

read -p "Donnez un nombre entier svp" NUM
for ((I=$NUM ; I>=1 ; I=I-1))
do
        echo "$I"
done

#EXERCICE 7
#!/bin/bash
for SYSTEMES in 1-Windows 2-MacOS 3-Linux 4-Unix 5-Quitter-Réponse
do
        echo "$SYSTEMES ?"
done
echo "Quel systemes voullez-vous utilisez ?"
read -p "Choisissez le numero de systeme SVP" NUMERO
if [ $NUMERO -eq 1 ]
then
        echo "Dommage...!"
elif [ $NUMERO -eq 2 ]
then
        echo "Peut Mieux Faire"
elif [ $NUMERO -eq 3 ]
then
        echo "Pas mal..!"

elif [ $NUMERO -eq 4 ]
then
        echo "SUPER"

elif [ $NUMERO -eq 9 ]
then
        echo "Merci Aurevoir"
        exit
else
        echo "Vous avez fait rentré un mauvais numero"
fi

#EXERCICE 8
#!/bin/bash

SIZE=10
echo "donnez les element du tableau"
read -a tab
if [ ${#tab[@]} -ne "$SIZE" ]
then
        echo "saisir le nombre d'element à ordoner"
else

        for ((i=0 ; i<$SIZE ; i++))
        do
                for((j=(($i+1)) ; j<$SIZE ; j++))
                do
                        if [ "${tab[$j]}" -lt "${tab[$i]}" ]
                        then

                                tab[$i]="${tab[$j]}"
                                tab[$j]="${tab[$i]}"
                        fi
                done
        done
        echo ${$tab[*]}
fi

#EXERCICE 9
#!/bin/bash
read -p "Donnez un nombre : " NUM
FACT=1

function factoriel(){
        if [ $NUM -lt 0 ]
        then
                echo "Il faut saisir un nombre entier possitif"
        else
                for ((I=1 ; I<=$NUM ; I++))
                do
                         FACT=$(($FACT*$I))
                done
                echo "Le factoriel de $NUM est :" $FACT
        fi
}
factoriel

#EXERCICE 10
#!/bin/bash

function debut(){
        read -p "Tappe la touche rentrer pour continuer"
}
debut

function nom_utilisateur(){
        read -p "Saisir votre nom d'identifiant SVP :" NOM
}
nom_utilisateur

function verification_du_nom_utilisateur(){
        if  grep "$NOM": /etc/passwd > /dev/null
        then
                echo "L'identifiant $NOM est existe deja"
        else
                echo "l'identifiant $NOM n'existe pas"
        fi
        debut
}
verification_du_nom_utilisateur

AN=1
while [ $AN -eq 1 ]
do
        clear
        echo "VOICI LES MENUS"
        echo "1.Verifier l'existence d'un utlisateur"
        echo "2.Connaitre l'uid d'un utilisateur"
        echo "q.Quitter"

        read -p "chosir un numero de menu :" MENU
        case "$MENU" in
                1)
                        nom_utilisateur
                        verification_du_nom_utilisateur
                        ;;
                2)
                        nom_utilisateur
                        id $NOM
                        debut
                        ;;
                q)
                        echo "Merci et aurevoir"
                        debut
                        AN=0
                        ;;
                *)
                        read -p "Il ya erreur de saisir, tappe la touche de ENTRER pour reprendre SVP"
                        debut
        esac
done
