#!/bin/bash

function pause(){
	echo "entrez la touche ENTRER pour continuer"
	read
}
pause

function verificationfichier_existe(){
	ls $1 2> /dev/null
	if [ $? -eq 0 ]
	then
		return 0
	else
		return 1
	fi
}
verificationfichier_existe

read -p "QUELLE EST ADRESSE OU NOM DE FIHCIER DONT LE DROIT VA ETRE MODIFIER :" FICHIER
verificationfichier_existe $FICHIER
if [ $? -eq 0 ]
then
	echo "le fichier $FICHIER existe bien"
else
	echo "le fichier $FICHIER n'existe pas"
	pause
	exit
fi

clear
echo "Donnez les modification des droits du proprietaire (r lecture; w ecriture ; x execution) :"
read DROIT_DU_PROPRIETAIRE
chmod u+$DROIT_DU_PROPRIETAIRE $FICHIER
echo ""
echo " les droit $DROIT_DU_PROPRIETAIRE proprietaire a été bien modifier sur le fichier $FICHIER"

echo ""
echo ""
echo "Donnez les modification des droits du groupe (r lecture; w ecriture ; x execution) :"
read DROIT_DU_GROUPE
chmod g+$DROIT_DU_GROUPE $FICHIER
echo ""
echo "les droits $DROIT_DU_GROUPE de groupe a été bien modifier sur le fichier $FICHIER"

echo""
echo""
echo "Donnez les modification des droits des autres (r lecture; w ecriture ; x execution) :"
read DROIT_DES_AUTRES
chmod o+$DROIT_DES_AUTRES $FICHIER
echo ""
echo "Les droits $DROIT_DES_AUTRES des autres a été bien modifé sur le fichier $FICHIER"
echo ""
