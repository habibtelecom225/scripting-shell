#!/bin/bash

function pause(){
	echo "entrez la touche ENTRER pour continuer"
	read
}
pause

function verificationfichier(){
	ls $1 2> /dev/null
	if [ $? -eq 0 ]
	then
		return 0
	else
		return 1
	fi
}
verificationfichier

B=1
while [ $B=1 ]
do

echo "VOICI LES MENUS :"
echo ""
echo "1.Creer un fichier"
echo "2.Supprimer un fichier"
echo ""
read -p "FAITES UN CHOIX DE NUMERO , OU TU TAPPE "Q" POUR QUITTER :" CHOIX

	case $CHOIX in
		1)
			read -p "Donner le nom de fichier à creer :" FICHIER
			verificationfichier $FICHIER
			if [ $? -eq 0 ]
			then
				echo "le fichier $FICHIER existe déja"
				pause
			else
				sudo touch $FICHIER
				echo "le fichier $FICHIER a été bien crée avec succès"
			fi
				;;
		2)
			read -p "Donner le nom de fichier à supprimer :" FICHIER
			verificationfichier $FICHIER
			if [ $? -eq 0 ]
			then
				sudo rm -Rf $FICHIER
				echo "le fichier $FICHIER a été bien supprimé avec succes"
			else
				echo "Le fichier $FICHIER n'existe pas"
				pause
			fi
			;;
		q)
			echo "Merci pour votre visites et Aurevoir"
			exit
			;;
		*)
			echo "Erreur de numero"
			pause
			;;
	esac
done
