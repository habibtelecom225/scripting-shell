#!/bin/bash

function pausse(){
	echo "tapper la touche d'ENTRE pour continuer"
	read
}
pausse

function verification_existence(){
	NOM_DES_UTILISATEURS={cat /etc/passwd  | cut -d:-f1}
	for UTILISATEUR in $NOM_DES_UTILISATEURS
	do
		if [ $1=$UTILISATEUR ]
		then
			return 1
		fi
	done
}
verification_existence

clear
echo "1.Creaction d'un utilisateur"
echo "2.Suppression d'un utilisateur"
read -p "VOICI LES MENUS , MERCI DE CHOISIR UN NUMERO :" CHOIX

case $CHOIX in
	1)
		echo "Donnez le nom d'utilisateur à crée"
		read NOM
		verification_existence $NOM
		if [ $? -eq 1 ]
		then
			echo "L'utilisateur $NOM existe déjà"
		else
			sudo useradd $NOM
			echo "L'utilisateur $NOM a été bien crée"
		fi
		;;
	2)
		echo "Donnez le nom de l'utilisateur à supprimer"
		read NOM
		verification_existence $NOM
		if [ $? -eq 1 ]
		then
			sudo userdel $NOM
			echo "L'utilisateur $NOM a été bien supprimé"
		else
			echo "L'utilisateur $NOM n'existe pas"
		fi
		;;
	*)
		echo "Erreur de choix"
		pausse
		;;
esac
