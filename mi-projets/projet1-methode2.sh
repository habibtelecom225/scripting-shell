#!/bin/bash

function pausse(){
	echo "tapper la touche d'ENTRE pour continuer"
	read
}
pausse

function verification_existence(){
	NOM_DES_UTILISATEURS={cat/etc/passwd  | cut -d:-f}
	for UTILISATEUR in $NOM_DES_UTILISATEURS
	do
		if [ $1=$UTILISATEUR ]
		then
			return 1
		fi
	done
}
verification_existence

read -p "Donnez le nom de l'utilisateur à verifier :" NOM
verification_existence $NOM
if [ $? -eq 1 ]
then
	echo " l'utilisateur $NOM exist déjà"
	exit
else
	echo " l'utlisateur $NOM n'existe pas"
fi
