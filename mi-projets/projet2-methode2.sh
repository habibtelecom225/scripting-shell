#!/bin/bash

function existence_groupe(){	
	LISTEGROUPE={sudo cat /etc/group | cut -d: -f1}
	for GROUPE in $LISTEGROUPE
	do
		if [ $1=$GROUPE ]
		then
			return 1
		fi
	done
}
existence_groupe

read -p "Ecrivez le nom du groupe :" NOMGROUPE
existence_groupe $NOMGROUPE
if [ $?=1 ]
then
	echo "Le groupe $NOMGROUPE existe déjà"
	exit 1
else
	echo "Le groupe $NOMGROUPE n'existe pas"
fi
