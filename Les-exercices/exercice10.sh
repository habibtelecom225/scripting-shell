#!/bin/bash

function debut(){
	read -p "Tappe la touche rentrer pour continuer"
}
debut

function nom_utilisateur(){
	read -p "Saisir votre nom d'identifiant SVP :" NOM
}
nom_utilisateur

function verification_du_nom_utilisateur(){
	if  grep "$NOM": /etc/passwd > /dev/null
	then
		echo "L'identifiant $NOM est existe deja"
	else
		echo "l'identifiant $NOM n'existe pas"
	fi
	debut
}
verification_du_nom_utilisateur

AN=1
while [ $AN -eq 1 ]
do
	clear
	echo "VOICI LES MENUS"
	echo "1.Verifier l'existence d'un utlisateur"
	echo "2.Connaitre l'uid d'un utilisateur"
	echo "q.Quitter"

	read -p "chosir un numero de menu :" MENU
	case "$MENU" in
		1)
			nom_utilisateur
			verification_du_nom_utilisateur
			;;
		2)
			nom_utilisateur
			id $NOM
			debut
			;;
		q)
			echo "Merci et aurevoir"
			debut
			AN=0
			;;
		*)
			read -p "Il ya erreur de saisir, tappe la touche de ENTRER pour reprendre SVP"
			debut
	esac
done
