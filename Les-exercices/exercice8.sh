#!/bin/bash

SIZE=10
echo "donnez les element du tableau"
read -a tab
if [ ${#tab[@]} -ne "$SIZE" ]
then
	echo "saisir le nombre d'element à ordoner"
else

	for ((i=0 ; i<$SIZE ; i++))
        do
		for((j=(($i+1)) ; j<$SIZE ; j++))
		do
			if [ "${tab[$j]}" -lt "${tab[$i]}" ]
			then
	
				tab[$i]="${tab[$j]}"
				tab[$j]="${tab[$i]}"
			fi
		done
	done
	echo ${$tab[*]}
fi
