#!/bin/bash

if [ $# -eq 0 ]
then
	echo "il n'ya aucun argument sur la ligne de commande"
else
	A=$(ls $1)
	for B in $A
	do
		if [ -d $B ]
		then
			echo "voici la liste sous-les repertoires du repertoire$1:" $B
		else
			echo "ce n'est pas un repertoire à la commande"
		fi
	done
fi
