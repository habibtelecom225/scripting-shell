#!/bin/bash
read -p "Donnez un nombre : " NUM
FACT=1

function factoriel(){
	if [ $NUM -lt 0 ]
	then
		echo "Il faut saisir un nombre entier possitif"
	else
		for ((I=1 ; I<=$NUM ; I++))
		do
			 FACT=$(($FACT*$I))
		done
		echo "Le factoriel de $NUM est :" $FACT
	fi
}
factoriel
